macroScript SuperSaver_v2
	category:"NSC Creative"
	toolTip: "NEW Super Saver"
	buttonText:"Super Saver"
	icon:#("SuperSaver",1)
	autoUndoEnabled:true
	
(
	callbacks.removeScripts #filePostSave id:#supersaver
	callbacks.removeScripts #filePostOpen id:#supersaver
	callbacks.removeScripts #systemPostNew id:#supersaver
	
	version = "v2.005"
	/*
	Version Notes v2.005"
	-- Fixed bug where Master Selected button was mastering the whole scene.
	
	
	Version Notes v2.004"
	-- Added functionality to get and set the position of the dialog when the file saved, opend or a new file is opened.
	-- Also added functionality to remove the callbacks if they are called but the position of the dialog is 0,0 (i.e not open.)
	
	
	Version Notes v2.003 
	-- Fixed an error where restartSuperSaver returned undefined -- Made restartSuperSaver global
	-- Fixed an error where the script would error within a Max file that was saved with a non conforming naming convention.
	-- Fixed a bug in master selected.
	
	*/


	try (DestroyDialog superSaverRollout) catch ()

	fn getDesc fileName = (
		fileName = trimRight fileName "_x.max"
		filteredFileNameArr = filterString fileName "_"
		if filteredFileNameArr.count >1 then ( return filteredFileNameArr[filteredFileNameArr.count-1] ) else ( return "")
	)
	
	fn getLargeNumber fileName = (
		fileName = substituteString fileName "_x" ""
		filteredFileNameArr = filterString fileName "_"
		if filteredFileNameArr.count >2 then ( largeNumber = filteredFileNameArr[filteredFileNameArr.count-2] ) else ( largeNumber = "")
		if ((largeNumber as integer) != undefined ) and ( (largeNumber as integer) != 0 ) then (return (largeNumber as integer) ) else ( return "" )
	)
	
	fn getSmallNumber fileName = (
		fileName = substituteString fileName "_x" ""
		filteredFileNameArr = filterString fileName "_"
		if filteredFileNameArr.count >2 then ( smallNumber = substituteString (filteredFileNameArr[filteredFileNameArr.count]) ".max" "" ) else ( smallNumber = "")
		if ((smallNumber as integer) != undefined ) and ( (smallNumber as integer) != 0 ) then (return (smallNumber as integer) ) else ( return "" )
	)
	
	fn assembleFileNameFromArray fileNameArr = (
		newFileName = ""
		for i = 1 to (fileNameArr.count - 1) do(
			newFileName += fileNameArr[i]+"_"
		)
		newFileName += fileNameArr[fileNameArr.count]
		return newFileName
	)
	
	global restartSuperSaver
	global superSaverPos
	global superSaverRollout
	
	fn restartSuperSaver = (
		superSaverPos = getDialogPos superSaverRollout
		if superSaverPos != [0,0] then (
			try (destroyDialog superSaverRollout) catch()
			try (createDialog superSaverRollout) catch()
			SetDialogPos superSaverRollout superSaverPos
		) else (
			callbacks.removeScripts #filePostSave id:#supersaver
			callbacks.removeScripts #filePostOpen id:#supersaver
			callbacks.removeScripts #systemPostNew id:#supersaver
		)
	)
	
	fn getFileName selectedDir = (
		if (doesFileExist selectedDir) != false then (
			if selectedDir.count != 0 then ( --if selected directory string isn't ""
				savePathArr = filterString selectedDir "\\"
				if savePathArr[savePathArr.count] == "wip" do ( --if the folder is a wip folder
					
					files = getFiles (selectedDir+"\\*.max")
					if files.count != 0 then ( -- if ther are max files in the directory
						
						filesExist = true
						
						filesSorted = sort files
						latestFile = filesSorted[filesSorted.count]
						latestFileName = (filterString latestFile "\\")[(filterString latestFile "\\").count]
						latestFileName = trimRight latestFileName ".max"
						latestFileName = trimRight latestFileName "_"
						latestFileNameArr = filterString latestFileName "_"
						
						if latestFileNameArr.count == 6 then ( -- if the latest file in the directory has a formatting that matches a shot scene
							
							small_version = latestFileNameArr[6]
							large_version = latestFileNameArr[4]
							shot_name = latestFileNameArr[1]
							pipeline_name = latestFileNameArr[2]
							scene_name = latestFileNameArr[3]
							scene_desc = latestFileNameArr[5]
							
							savefilename = latestFileNameArr[1]+"_"+latestFileNameArr[2]+"_"+latestFileNameArr[3]+"_"+latestFileNameArr[4]+"_"+latestFileNameArr[5]+"_"+latestFileNameArr[6]+".max"
-- 							print "is shot"
						) else if ( latestFileNameArr.count == 5 ) then ( -- if the latest file in the directory has a formatting that maches an asset scene
							small_version = latestFileNameArr[5]
							large_version = latestFileNameArr[3]
							shot_name = latestFileNameArr[1]
							pipeline_name = latestFileNameArr[2]
							scene_desc = latestFileNameArr[4]
							
							savefilename = latestFileNameArr[1]+"_"+latestFileNameArr[2]+"_"+latestFileNameArr[3]+"_"+latestFileNameArr[4]+"_"+latestFileNameArr[5]+".max"
-- 							print "is asset"
						) else ( -- there are files but they don't match the correct naming convention.
-- 							print "no valid file exists in this directory"
							filesExist = false
							saveFileName = ""
						)
					) else ( -- there were no files in the directory
-- 						print "no files in directory"
						filesExist = false
						savefilename= ""
					)
						
					if filesExist == false do( -- if no files are in the directory ( or no files with the correct naming convention) then deduce name from path
						
						savePathArr = filterString selectedDir "\\"
						
						isAsset = false
						for i in savePathArr do(
							if i == "_assets" do( isAsset = true)
						)
						
						if isAsset == false then ( -- is not an asset (is a shot)	
							l = savePathArr.count
							scene_name = savePathArr[l-2]
							pipeline_name = savePathArr[l-3]
							shot_name = savePathArr[l-4]
							savefilename = (shot_name+"_"+pipeline_name+"_"+scene_name)+"_001_initialsave_01.max"
							
						) else ( -- is an asset
							l = savePathArr.count
							--scene_name = savePathArr[l-2]
							pipeline_name = savePathArr[l-2]
							shot_name = savePathArr[l-3]
							savefilename = (shot_name+"_"+pipeline_name)+"_001_initialsave_01.max"
						)
-- 						if doesFileExist (selectedDir+"\\"+savefilename) == false then (
-- 							savefilename = savefilename
-- 						) else ( Print "File already exists with the same name." )	
					)
				)
			) else ( saveFileName = "" ) -- there is no specfied directory
		) else ( saveFileName = "" )
		return saveFileName
	)

	rollout superSaverRollout "SuperSaver Beta" width:528 height:88
	(
		edittext 'edtFilePath' "" pos:[8,8] width:400 height:17 align:#left
		button 'btnBrowse' "..." pos:[416,8] width:72 height:17 align:#left
		button 'btnSave' "Save" pos:[232,32] width:56 height:17 align:#left
		label 'lblFilename' "Label" pos:[80,56] width:272 height:19 align:#left
		edittext 'edtDescription' "Description:" pos:[16,32] width:213 height:17 align:#left
		button 'btnIncrement' "+" pos:[296,32] width:16 height:17 align:#left
		button 'btnMaster' "Master" pos:[416,32] width:72 height:17 align:#left
		button 'btnExplorer' "E" pos:[496,8] width:24 height:17 align:#left
		label 'lblFileNameLabel' "Filename:" pos:[16,56] width:56 height:19 align:#left
		button 'btnMasterSelected' "m" pos:[496,32] width:24 height:17 align:#left
		dropdownList 'ddlMasterArr' "" pos:[320,32] width:88 height:21 items:#("New Master") align:#left
		label 'lblVersion' "v0.000" pos:[480,64] width:40 height:16 align:#left

		on superSaverRollout open do
		(
			lblVersion.text = version
			if maxFileName != "" then (
				-- get list of masters
				maxpathArr = filterstring maxfilepath "\\"
				if toLower(maxpathArr[maxpathArr.count]) == "wip" then (
					btnMaster.enabled = true
					btnMasterSelected.enabled = true
				) else (
					btnMaster.enabled = false
					btnMasterSelected.enabled = false
				)
				masterPath = trimRight maxFilePath "wip\\"
				masterPath += "\\masters\\"
				masterFileArr = getFiles (masterPath+"*.max")
				arr = superSaverRollout.ddlMasterArr.items
				for i in masterFileArr do(
					temp = i
					tempArr = filterString temp "\\"
					i = tempArr[tempArr.count]
					appendIfUnique arr i
				)
				
			if (getDesc MaxFileName) == "" do	(
				btnMaster.enabled = false
				btnMasterSelected.enabled = false
			)
				
				superSaverRollout.ddlMasterArr.items = (sort arr)
				
				-- get file name
				fileName = maxFileName
				lblFileName.text = substituteString fileName ".max" ""
				filePath = maxFilePath
				edtFilePath.text = filePath
				edtDescription.text = getDesc maxFileName 
			) else (
				btnSave.enabled = false
				btnIncrement.enabled = false
				edtFilePath.text = ""
				edtDescription.text = ""
				lblFilename.text = "Please enter a directory ^__^"
			)
		)
		on edtFilePath changed txt do
		(
			if txt != "" do (
				if txt[txt.count] == "\\" do ( txt = trimRight txt "\\" )
				fileName = getFileName txt
				if fileName != undefined then(
					if doesDirectoryExist txt == true then (
						if (findString fileName "initialsave") != undefined then ( -- if its not a new file
							fileName = getFileName txt
							lblFileName.text = trimRight fileName ".max"
							edtDescription.text = getDesc fileName
							btnSave.enabled = true
							btnIncrement.enabled = true
						) else if maxFileName != fileName then (-- if it doesnt match the file name
							fileArr  = getFiles (txt+"\\*.max")
							if fileArr.count != 0 do lblFileName.text = "Directory and filename mismatch, please save manually."
								btnSave.enabled = false
								btnIncrement.enabled = false
						) else ( -- else everything else.
							fileName = getFileName txt
							lblFileName.text = trimRight fileName ".max"
							edtDescription.text = getDesc fileName
							btnSave.enabled = true
							btnIncrement.enabled = true
						)
					) else (
						btnSave.enabled = false
						btnIncrement.enabled = false
					)
				) else ( lblFileName.text = "001_"+edtDescription.text+"_01" )
			)
			pathArr = filterstring txt "\\"
			if pathArr.count != 0 do (
				if pathArr[pathArr.count] == "wip" then (
					btnMaster.enabled = true
					btnMasterSelected.enabled = true
				) else (
					btnMaster.enabled = false
					btnMasterSelected.enabled = false
				)
			)
		)
		on btnBrowse pressed do
		(
				txt = getSavePath caption:"Select directory"
				if txt != "" do (
					edtFilePath.text = txt
					if txt[txt.count] == "\\" do ( txt = trimRight txt "\\" )
					fileName = getFileName txt	
					if doesDirectoryExist txt == true do(
						if (findString fileName "initialsave") != undefined then ( -- if its a new file
							fileName = getFileName txt
							lblFileName.text = trimRight fileName ".max"
							edtDescription.text = getDesc fileName
							btnSave.enabled = true
							btnIncrement.enabled = true
						) else if maxFileName != fileName then (-- if it doesnt match the file name
							fileArr  = getFiles (txt+"\\*.max")
							if fileArr.count != 0 do lblFileName.text = "Directory and filename mismatch, please save manually."
								btnSave.enabled = false
								btnIncrement.enabled = false
						) else ( -- else everything else.
							fileName = getFileName txt
							lblFileName.text = trimRight fileName ".max"
							edtDescription.text = getDesc fileName
							btnSave.enabled = true
							btnIncrement.enabled = true
						)
					)
				)
			)
		on btnSave pressed do
		(
			filePath = superSaverRollout.edtFilePath.text
			fileName = superSaverRollout.lblFileName.text+".max"
			if filePath != "" do (
				if (filePath[filePath.count] != "\\") then (
					savePath = (filePath+"\\"+fileName)
				) else (
					savePath = (filePath+fileName)
				)

				callbacks.addScript #filePostSave "restartSuperSaver()" id:#supersaver
				callbacks.addScript #filePostOpen "restartSuperSaver()" id:#supersaver
				callbacks.addScript #systemPostNew "restartSuperSaver()" id:#supersaver
				saveMaxFile savePath
			)
		)
		on btnMaster pressed do(
			pathArr = filterString maxFilePath "\\"
			if pathArr[pathArr.count] == "wip" do (
				masterArr = superSaverRollout.ddlMasterArr.items
				sel = superSaverRollout.ddlMasterArr.selection
				filePath = edtFilePath.text
				filePath = trimRight filePath "\\"
				if filePath != "" do(
					masterPath = (trimRight filePath "wip")+"masters"
					if sel == 1 then (
						fileName = trimRight (superSaverRollout.lblFileName.text) "_x"
-- 							print fileName
						fileNameArr = filterString fileName "_"
-- 							fileNameArr[0]
-- 							print FileNameArr
						l = fileNameArr.count
						print l
						if l != 1 do (
							for i in l to l-2 by - 1 do (deleteItem fileNameArr i)
						)
						--prefix = assembleFileNameFromArray fileNameArr
						masterSavePath = getSaveFileName caption:"Save Master..." filename: fileNameArr[1] initialDir:masterPath
						if masterSavePath != undefined do(
							masterSavePath += ".max"
							masterSavePathArr = filterString masterSavePath "\\"
							masterSaveName = masterSavePathArr[masterSavePathArr.count]
						)
					) else (
						masterSavePath = masterPath+"\\"+masterArr[sel]
					)
					if masterSavePath != undefined do (
						appendIfUnique masterArr masterSaveName
						saveMaxFile masterSavePath useNewFile:false
			-- 			superSaverRollout.ddlMasterArr.items = masterArr
						fileName = (trimRight maxFileName ".max")
						smallNumber = (getSmallNumber fileName)+1
						smallNumber = formattedPrint smallNumber format:"02d"
						fileNameArr = filterString fileName "_"
						deleteitem filenamearr filenamearr.count
						append filenamearr smallnumber
						newfilename = (assemblefilenamefromarray filenamearr)+"_x.max"
						
						callbacks.removeScripts #filePostSave id:#supersaver
						callbacks.removeScripts #filePostOpen id:#supersaver
						callbacks.removeScripts #systemPostNew id:#supersaver
						savemaxfile (maxfilepath+newfilename)
						restartSuperSaver()
					)
				)
			)
		)
		on btnMasterSelected pressed do( -- this should really be a function as its taken from btnMaster
			if ((selection as array).count) != 0 do(
				pathArr = filterString maxFilePath "\\"
				if pathArr[pathArr.count] == "wip" do (
					masterArr = superSaverRollout.ddlMasterArr.items
					sel = superSaverRollout.ddlMasterArr.selection
					filePath = edtFilePath.text
					filePath = trimRight filePath "\\"
					if filePath != "" do(
						masterPath = (trimRight filePath "wip")+"masters"
						if sel == 1 then (
							fileName = trimRight (superSaverRollout.lblFileName.text) "_x"
							fileNameArr = filterString fileName "_"
							l = fileNameArr.count
							print l
							if l != 1 do (
								for i in l to l-2 by - 1 do (deleteItem fileNameArr i)
							)
							masterSavePath = getSaveFileName caption:"Save Master..." filename: fileNameArr[1] initialDir:masterPath
							if masterSavePath != undefined do(
								masterSavePath += ".max"
								masterSavePathArr = filterString masterSavePath "\\"
								masterSaveName = masterSavePathArr[masterSavePathArr.count]
							)
						) else (
							masterSavePath = masterPath+"\\"+masterArr[sel]
						)
						if masterSavePath != undefined do (
							appendIfUnique masterArr masterSaveName
							saveNodes selection masterSavePath
							fileName = (trimRight maxFileName ".max")
							smallNumber = (getSmallNumber fileName)+1
							smallNumber = formattedPrint smallNumber format:"02d"
							fileNameArr = filterString fileName "_"
							deleteitem filenamearr filenamearr.count
							append filenamearr smallnumber
							newfilename = (assemblefilenamefromarray filenamearr)+"_x.max"
							
							callbacks.removeScripts #filePostSave id:#supersaver
							callbacks.removeScripts #filePostOpen id:#supersaver
							callbacks.removeScripts #systemPostNew id:#supersaver
							savemaxfile (maxfilepath+newfilename)
							restartSuperSaver()
						)
					)
				)
			)
		)
		on edtDescription changed txt do
		(
			if maxFileName != "" then (
				if txt != "" then(				
					fileName = (substituteString maxFileName ".max" "")
					fileName = trimRight fileName "_x"
					fileNameArr = filterString fileName "_"
-- 					fileNameArr[fileNameArr.count] = fileNameArr[fileNameArr.count]
					if fileNameArr.count > 1 do (
						fileNameArr[fileNameArr.count-1] = txt
						newFileName = assembleFileNameFromArray fileNameArr
					)
				) else (
					newFileName = (substituteString maxFileName ".max" "")
				)
				
				if (substituteString maxFileName ".max" "") != newFileName do (
					largeNumber = getLargeNumber maxFileName
					if largeNumber != "" do(
						largeNumber = largeNumber+1
						largeNumberStr = formattedPrint largeNumber format:"03d"
						fileNameArr = filterString newFileName "_"
						fileNameArr[fileNameArr.count-2] = largeNumberStr
						fileNameArr[fileNameArr.count] = "01"
						newFileName = assembleFileNameFromArray fileNameArr
					)
				)
				
				if newFileName != undefiend then (
					edtDescription.text = toLower(substituteString txt " " "-")
					lblFileName.text = toLower(substituteString newFileName " " "-")
				) else (
					fileName  = getFileName maxFilePath
					if fileName != undefiend do (
						superSaverRollout.lblFileName.text = fileName
					)
				)
			) else (
				superSaverRollout.lblFileName.text = "001_"+(edtDescription.text = toLower(substituteString txt " " "-"))+"_01"
			)
		)
		on btnIncrement pressed do
		(
				--actionMan.executeAction 0 "286" --File: Save Incremental (+)
			if maxfilename != "" do (
				fileName = trimRight maxfilename ".max"
				fileName = trimRight fileName "_x"
				smallnumber = (getSmallNumber filename)+1
				smallNumberStr = formattedPrint smallnumber  format:"02d"
				fileNameArr = FilterString fileName "_"
				filenamearr[filenamearr.count] = smallnumberstr
				savename = (assemblefilenamefromarray filenamearr)+".max"
				
				callbacks.removeScripts #filePostSave id:#supersaver
				callbacks.removeScripts #filePostOpen id:#supersaver
				callbacks.removeScripts #systemPostNew id:#supersaver
				saveMaxFile (maxfilepath+savename)
				restartSuperSaver()
			)
			
		)
		on btnExplorer pressed do( -- On E buttion pressed open maxFilePath in explorer
			if edtFilePath.text != "" do (
				if doesfileexist edtFilePath.text == true do (
					ShellLaunch "explorer.exe" edtFilePath.text
				)
			)
		)
	)

	callbacks.addScript #filePostSave "restartSuperSaver()" id:#supersaver
	callbacks.addScript #filePostOpen "restartSuperSaver()" id:#supersaver
	callbacks.addScript #systemPostNew "restartSuperSaver()" id:#supersaver


	CreateDialog superSaverRollout
-- 	try (SetDialogPos superSaverRollout superSaverPos) catch()
)